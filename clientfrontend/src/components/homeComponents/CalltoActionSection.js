import React from "react";

const CalltoActionSection = () => {
  return (
    <div className="subscribe-section bg-with-black">
      <div className="container">
        <div className="row">
          <div className="col-xs-12">
            <div className="subscribe-head">
              <h2>Avez-vous besoin de conseils?</h2>
              <p>Inscrivez-vous gratuitement et obtenez les derniers conseils.</p>
              <form className="form-section">
                <input placeholder="Votre Adresse mail..." name="email" type="email" />
                <input value="Oui je veux bien!" name="subscribe" type="submit" />
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default CalltoActionSection;
