import React from "react";
import "./App.css";
import "./responsive.css";
import "react-toastify/dist/ReactToastify.css";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Home from "./views/Home";
import SingleProduct from "./views/SingleProduct";
import Login from "./views/Login";
import Register from "./views/Register";
import Cart from "./views/Cart";
import Shipping from "./views/Shipping";
import Profile from "./views/Profile";
import Payment from "./views/Payment";
import PlaceOrder from "./views/PlaceOrder";
import Order from "./views/Order";
import NotFound from "./views/NotFound";
import PrivateRouter from "./PrivateRouter";

const App = () => {
  return (
    <Router>
      <Switch>
        <Route path="/" component={Home} exact />
        <Route path="/search/:keyword" component={Home} exact />
        <Route path="/page/:pagenumber" component={Home} exact />
        <Route
          path="/search/:keyword/page/:pageNumber"
          component={Home}
          exact
        />
        <Route path="/products/:id" component={SingleProduct} />
        <Route path="/login" component={Login} />
        <Route path="/register" component={Register} />
        <PrivateRouter path="/profile" component={Profile} />
        <Route path="/cart/:id?" component={Cart} />
        <PrivateRouter path="/shipping" component={Shipping} />
        <PrivateRouter path="/payment" component={Payment} />
        <PrivateRouter path="/placeorder" component={PlaceOrder} />
        <PrivateRouter path="/order/:id" component={Order} />
        <Route path="*" component={NotFound} />
      </Switch>
    </Router>
  );
};

export default App;
