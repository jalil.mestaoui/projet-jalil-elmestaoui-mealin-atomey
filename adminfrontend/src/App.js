import React, { useEffect } from "react";
import "./App.css";
import "./responsive.css";
import "react-toastify/dist/ReactToastify.css";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Home from "./views/Home";
import Product from "./views/product";
import Categories from "./views/Categories";
import Order from "./views/Order";
import OrderDetail from "./views/OrderDetails";
import AddProduct from "./views/AddProduct";
import Login from "./views/Login";
import Users from "./views/Users";
import ProductEdit from "./views/ProductEdit";
import NotFound from "./views/NotFound";
import PrivateRouter from "./PrivateRouter";
import { useDispatch, useSelector } from "react-redux";
import { listProducts } from "./Redux/Actions/ProductActions";
import { listOrders } from "./Redux/Actions/OrderActions";

function App() {
  const dispatch = useDispatch();

  const userLogin = useSelector((state) => state.userLogin);
  const { userInfo } = userLogin;

  useEffect(() => {
    if (userInfo && userInfo.isAdmin) {
      dispatch(listProducts());
      dispatch(listOrders());
    }
  }, [dispatch, userInfo]);

  return (
    <>
      <Router>
        <Switch>
          <PrivateRouter path="/" component={Home} exact />
          <PrivateRouter path="/products" component={Product} />
          <PrivateRouter path="/category" component={Categories} />
          <PrivateRouter path="/orders" component={Order} />
          <PrivateRouter path="/order/:id" component={OrderDetail} />
          <PrivateRouter path="/addproduct" component={AddProduct} />
          <PrivateRouter path="/users" component={Users} />
          <PrivateRouter
            path="/product/:id/edit"
            component={ProductEdit}
          />
          <Route path="/login" component={Login} />
          <PrivateRouter path="*" component={NotFound} />
        </Switch>
      </Router>
    </>
  );
}

export default App;
